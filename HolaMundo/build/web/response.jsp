<%-- 
    Document   : response
    Created on : 21/02/2018, 08:51:31 PM
    Author     : mareyes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="shortcut icon" href="ico.ico">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/Style.css">
        <!--<link rel="stylesheet" href="fontawesome-free-5.0.6/web-fonts-with-css/CSS/fontawesome.min.css"> -->
        <script src="js/jquery-3.3.1.js"></script>
        <script src="js/bootstrap.min.js"></script>
        
        <nav class="navbar navbar-default" role="navigation">
            <div class="navigation-header">                
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">                            
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                    </button>  
                <a class="navbar-brand" href="https://www.gitlab.com/Rezamar"><img id="logo" src="mareyes.png" id="logo"></a>
            </div>
            
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="NPrimos.jsp">Números primos</a></li>
                    <li><a href="Formulario.jsp">Formulario</a></li>
                    <li><a href="Figuras.jsp">Cálculo de Figuras</a></li>                   
                </ul>
            </div>
        </nav>        
    </head>
    <body>
        <jsp:useBean id="mifrijol" scope="session" class="org.mypackage.hello.NameHandler" />
        <jsp:setProperty name="mifrijol" property="name" />
        <h1>Hola mundo, <jsp:getProperty name="mifrijol" property="name" /> !</h1>
    </body>
</html>
