<%-- 
    Document   : Principal
    Created on : 28/02/2018, 07:37:03 PM
    Author     : Jess
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="CSS/estilo.css" rel="stylesheet" type="text/css">
        <title>Formulario - Servlet</title>
    </head>
    <body class="fondo">

<ul id="button">
	<li><a href="#">Formulario</a></li>
	<li><a href="tienda.jsp">Tienda</a></li>
</ul>

<br><br><br>
<center>
<h1>Registro</h1>



<form class="formulario" name="frmUsuario" action="./autenticacionServlet" method="post">
    <ul>
        <li>
            <label for='nombre' class="etiqueta">Nombre</label>
            <input class="input" type='text' name='nombre' id='nombre' required />
            
        </li>
        <br>
        <li>
            <label for='telefono' class="etiqueta">Telefono</label>
            <input class="input" type='text' name='telefono' id='telefono' required/>
        </li>
        <br>
        <li>
            <label for='edad' class="etiqueta">Edad</label>
            <input class="input" type='text' name='edad' id='edad' maxlength="3" required/>
        </li>
        <br>
         <li>
            <label for='email' class="etiqueta">E-mail</label>
            <input class="input" type='text' name='email' id='email' required />
        </li>
         <br>
         <li>
            <label for='email2' class="etiqueta">Confirmar E-mail</label>
            <input class="input" type='text' name='email2' id='email2' required />
        </li>
        <br>
        <li>
        	<label for="passwd" class="etiqueta">Contraseña:</label>
        	<input class="input" type="password" name="passwd" id="passwd" minlength="5" required>
        </li>
        <br>
        <li>
        	<label for="passwd2" class="etiqueta">Confirmar contraseña:</label>
        	<input class="input" type="password" id="passwd2" minlength="5" required>
        </li>
        <br>
		<li>
            <label for='sexo' class="etiqueta">Sexo</label>
            <select class="caja" id='sexo' name='sexo'/>
			<option value=''>Seleccione</option>
			<option>Femenino</option>
			<option>Masculino</option>
			</select>
        </li>
        <br>
		<li>
            <label class="condiciones">
            <input type='checkbox' value='SI' id='acepto'/>
			He leido y acepto las condiciones.
			</label>
        </li>
        <br><br>
    </ul>
    
    <center>
    	<button class="botonRegistro">Registrarse</button>
    </center>
</form>

</center>


</body>
</html>
</html>

