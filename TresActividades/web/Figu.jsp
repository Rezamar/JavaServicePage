<%-- 
    Document   : Figu
    Created on : 25/02/2018, 06:45:03 PM
    Author     : mareyes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">       
        <title>Figueuas</title>
        <link rel="shortcut icon" href="ico.ico">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/Style.css">
        <!--<link rel="stylesheet" href="fontawesome-free-5.0.6/web-fonts-with-css/CSS/fontawesome.min.css"> -->
        <script src="js/jquery-3.3.1.js"></script>
        <script src="js/bootstrap.min.js"></script>
                        
        <nav class="navbar navbar-default" role="navigation">
            <div class="navigation-header">                
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">                            
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                    </button>  
                <a class="navbar-brand" href="https://www.gitlab.com/Rezamar"><img id="logo" src="mareyes.png" id="logo"></a>
            </div>
            
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="NPrimos.jsp">Números primos</a></li>
                    <li><a href="Formulario.jsp">Formulario</a></li>
                    <li><a href="Figu.jsp">Cálculo de Figuras</a></li> 
                    <li><a href="Formulario2.jsp">Formulario2</a></li>
                    <li><a href="refaccionaria.jsp">Refaccionaria</a></li>
                </ul>
            </div>
        </nav>
    </head>
    <body>                
        <form method="post">
        <img src="triangulo.png" align="left" class="triangulo" width="100" height="100"/>
        <input type="text" name="altura" id="altura" class="figura1" placeholder="altura" id="id1">               
        <input type="text" name="base" id="base" class="figura1" placeholder="base" id="id2">        
        <input type="text" name="area" id="area" class="area1" placeholder="area" readonly="readonly">
        <input type="text" name="perimetro" id="perimetro" class="perimetro1" placeholder="perimetro" readonly="readonly">               
        <input type="radio" name="figura1" id="selectfi" class="selectri" value="1" checked="checked">        
        <input type="checkbox" name="campotriangulo" id="tri1" class="calcuarea">
        <input type="checkbox" name="campotriangulo" class="calcuperi">
        <input type="checkbox" name="ambos" class="periarea">
        <input type="submit" name="calcular" class="caltrian" value="Calculartrian" dir="Ejecutor2.jsp"/>        
        <!--Cuadrado-->
        <img src="cuadrado.jpg" class="cuadrado" width="100" height="100"/>
        <input type="text" name="altura1" id="altura1" class="figura2" placeholder="LadoxLado" id="id3">                               
        <input type="text" name="area1" class="area2" placeholder="area" readonly="readonly">
        <input type="text" name="perimetro1" class="perimetro2" placeholder="perimetro" readonly="readonly">
        <input type="submit" name="calcular1" class="calcuad" value="Calcularcuad" dir="Ejecutor3.jsp">
        <input type="radio" name="figura1" class="selecuad" value="2">
        <input type="checkbox" name="campocuadrado" id="cua1" class="calcuarea1">
        <input type="checkbox" name="campocuadrado" class="calcuperi1">
        <input type="checkbox" name="ambos1" class="periarea1">
        <!--Rectangulo-->
        <img src="rectangulo.jpg" class="rectangulo" width="150" height="100"/>                       
        <input type="text" name="base2" class="figura3" placeholder="base" id="id4">        
        <input type="text" name="altura2" class="figura3" placeholder="altura" id="id5">                       
        <input type="text" name="area2" class="area3" placeholder="area" readonly="readonly">
        <input type="text" name="perimetro2" class="perimetro3" placeholder="perimetro" readonly="readonly">
        <input type="radio" name="figura1" class="selectrec" value="3">
        <input type="checkbox" name="camporectangulo" id="cua1" class="calcuarea2">
        <input type="checkbox" name="camporectangulo" class="calcuperi2">
        <input type="checkbox" name="ambos2" class="periarea2">
        <input type="submit" name="calcular2" class="calrec" value="Calcularrec" dir="Ejecutor4.jsp">                           
        </form>  
        
        <script>                
        $("input:text").prop("disabled", true);

            $("input:radio").on("click", function() {
                $("input:text").prop("disabled", true);
                $("input.figura" + $(this).val()).prop("disabled", false);
            });                            
        </script>    
        
        <script>
            $(document).ready(function()
            {
                $("input[type=submit]").click(function()
                {
                    var accion = $(this).attr('dir');
                    $('form').attr('action', accion);
                    $('form').submit();
                });
            });
        </script>
    </body>
</html>
