/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ultimasactividades;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mareyes
 */
@WebServlet(name = "Refaccionaria", urlPatterns = {"/Refaccionaria"})
public class Refaccionaria extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
             String bate = request.getParameter("bateria");
             String acei = request.getParameter("aceite");             
             String Lbris = request.getParameter("limpiador");
             String Btiempo = request.getParameter("banda");             
             
             int p1 = 20;
             int p2 = 20;
             int p3 = 20;
             int p4 = 20;             
             int cant1 = Integer.parseInt(bate);
             int cant2 = Integer.parseInt(acei);
             int cant3 = Integer.parseInt(Lbris);
             int cant4 = Integer.parseInt(Btiempo);             
             
             float total = (p1 * cant1) + (p2 * cant2) + (p3 * cant3) + (p4 * cant4);                                             
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");            
            out.println("<head>");
            out.println("<title>Respuesta</title>");
            out.println("<meta charset=\"utf-8\">");
            out.println("<link rel=\"shortcut icon\" href='./ico.ico'\">");
            out.println("<link href='./css/Style.css' rel='stylesheet' type='text/css'>");                        
            out.println("<script src='./js/jquery-3.3.1.js'\"></script>");
            out.println("<script src='./js/bootstrap.min.js'\"></script>");
            out.println("<nav class=\"navbar navbar-default\" role=\"navigation\">");
            out.println("<div class=\"navigation-header\">                ");
            out.println("<button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">");
            out.println("<span class=\"icon-bar\"></span>");
            out.println("<span class=\"icon-bar\"></span>");
            out.println("<span class=\"icon-bar\"></span>");
            out.println("</button>");
            out.println("<a class=\"navbar-brand\" href=\"https://www.gitlab.com/Rezamar\"><img id=\"logo\" src=\"mareyes.png\" id=\"logo\"></a>");
            out.println("</div>");
            out.println("<div class=\"navbar-collapse collapse\">");
            out.println("<ul class=\"nav navbar-nav navbar-left\">");
            out.println("<li><a href=\"NPrimos.jsp\">Números primos</a></li>");
            out.println("<li><a href=\"Formulario.jsp\">Formulario1</a></li>");
            out.println("<li><a href=\"Figu.jsp\">Cálculo de Figuras</a></li>");
            out.println("<li><a href=\"Formulario2.jsp\">Formulario2</a></li>");
            out.println("<li><a href=\"tienda.jsp\">Refaccionaria</a></li>");
            out.println("</ul>");
            out.println("</div>");
            out.println("</nav>"); 
            out.println("</head>");
            out.println("<body class='fondo'>");            
            out.println("<ul id='button'>");            
            out.println("</ul>");
            out.println("<br><br><br>");
            
            out.println("<h1>Total</h1>");
            out.println("<form class='formulario'>");                                    
                                    
            out.println("<h2>Su pago: $" + total + " Gracias!</h2>");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
