/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author mareyes
 */
public class Consultas extends Conexion{
    
    public boolean autenticacion(String usuario, String contrasenia){
        PreparedStatement pst = null;
        ResultSet rs = null;
        
        try{
            String consulta = "select * from usuarios where usuario = ? and password = ?";
            pst = getConexion().prepareStatement(consulta);
            pst.setString(1, usuario);
            pst.setString(2, contrasenia);
            /*pst.setString(3, apellido);
            pst.setString(4, nombre);
            pst.setString(4, email);*/
            rs = pst.executeQuery();
            
            if(rs.absolute(1)){
                return true;    //Si finaliza con un valor no null retorna los parametros
            }
        }catch(Exception e){
            System.err.println("ERROR"+ e);
        }finally{
            try{
                if(getConexion() != null) getConexion().close();
                if(pst != null) pst.close();
                if(rs != null) rs.close();
            }catch(Exception e){
                
            }
        }
        return false;
    }
      
    public boolean registrar(String nombre, String apellido, String password, String usuario, String email){
        
        PreparedStatement pst = null;
        
        try{
            String consulta = "insert into usuarios (nombre,apellido,usuario,password,email) values (?,?,?,?,?)"; 
            pst = getConexion().prepareStatement(consulta);
            pst.setString(1, nombre);
            pst.setString(2, apellido);
            pst.setString(3, password);
            pst.setString(4, usuario);
            pst.setString(5, email);
            
            if(pst.executeUpdate() == 1){      
                return true;
            }
        }catch(Exception ex){
            System.err.println("Error" + ex);
        }finally{
            try{
                if(getConexion() != null) getConexion().close();
                if(pst != null) pst.close();
            }catch(Exception e){
            System.err.println("Error" + e);    
            }
        }
        
        return false;
    }
    
    /*public static void main(String[] args)                Para la autenticación de manera compilada en Java
    {
        Consultas co = new Consultas();
        System.out.println(co.autenticacion("mareyes","fish3rmaurici"));            Descomentar solamente una a la vez
    }*//*
    public static void main(String[] args)
    {                                                         Para insertar los datos de manera manual
        Consultas co = new Consultas();
        System.out.println(co.registrar("Fernando","delpaso","fercho","123456","putostodos@aol.com"));
    }*/
}
